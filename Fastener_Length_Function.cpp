/*
 * Fastener_Lenght_Function.cpp
 * Created: 11-7-2018 11:12:13
 *  Author: RK
 */ 

//Includes
#include "Main_Siemens_Automation_824MSPe.h"

//Local Variables.
int Fastener_Length_Zone;

//Timer Variables.
unsigned long Timer_11;
//Set timer to true when timer is finished.
bool TimedOut_11 = false;
//Amount of time the Timer is active.
unsigned long INTERVAL_11;
//Fastener Detection Delay Var.
bool FL_Delay;
//Fastener Detection Delay Time.
int FL_Delay_Time = 800;

//CET set value.
int CetValue_Fastener_Length_Function;

//Setup Code.
void Setup_Fastener_Length_Detection_Function(void)
{
	//Timer initialization.
	TimedOut_11 = false;
	//Start Timer.
	Timer_11 = millis();
}

//Function Code.
void Fastener_Length_Function(void)
{	
	//Read analog CET value.
	CetValue_Fastener_Length_Function = Analog_CET;
	
	if (Fastener_Length_HMI == true && !FLD_Measurment_Active == true && Tooling_Mode_HMI == 2)
	{	
		//Fastener Length Detection check.
		//Check if fastener is too Long.
		if (Fastener_Length_Error == true || (Toolingcontact_Switched_State == true && (CetValue_Fastener_Length_Function > (Fastener_Length_Calibrated_Value_HMI + Fastener_Length_Sensitifity_HMI)) && digitalRead(DOWN_SOLENOID) == HIGH) || FL_Delay == true)
		{
			Fastener_Length_Error = true;
		}
		//Check if fastener is too short.
		else if (Fastener_Length_Error == true || (Toolingcontact_Switched_State == false && (CetValue_Fastener_Length_Function < (Fastener_Length_Calibrated_Value_HMI - Fastener_Length_Sensitifity_HMI)) && digitalRead(DOWN_SOLENOID) == HIGH) || FL_Delay == true)
		{
			Fastener_Length_Error = true;
		}
	}
	//Ensures that no fastener Length error will be generated below "Fastener Length Calibrated Value HMI"" during Setup Stroke.
	if (CetValue_Fastener_Length_Function > (Fastener_Length_Calibrated_Value_HMI + 100))
	{
		FLD_Measurment_Active = false;
	}
	//Read coils. Reset FL error. MCU is master at reset. HMI will get only ACK.
	if (!Safety_Sensor_Error == true && !SSSF_Safety_Sensor_Error == true && !TPS_Error == true && !Fastener_Detection_Error == true && !Fastener_Length_Error == true && !FD_Retry_Set == true && !Shuttle_Extend_Error == true && !Shuttle_Retract_Error == true)
	{		
		Error_Set_HMI = 0;
		Error_Number_HMI = 0;
	}
	//Write coils. Set HMI into error mode. Display FL error by error number 17.
	if (Fastener_Length_Error == true)
	{
		Read_Coil_48 = false;
		Write_Coil_48 = true;
		Read_Coil_73 = false;
		Write_Coil_73 = true;
		Error_Set_HMI = 1;
		Error_Number_HMI = 17;
	}
	//Fastener Length Timer. Delay Error Reset Signal (500ms).
	if ((!TimedOut_11) && ((millis() - Timer_11) > INTERVAL_11))
	{
		//Timed out to deactivate timer.
		TimedOut_11 = true;

		//Check if FL Delay is activated.
		if (FL_Delay == true)
		{
			//De-active Timer.
			FL_Delay = false;
		}
	}
	//Set FL Delay Time.
	{
		INTERVAL_11 = FL_Delay_Time;
	}
	//Timer Start. Activate FL Delay Timer.
	if (Fastener_Length_Error == true && (CetValue_Fastener_Length_Function < TOS) && (INTERVAL_11 > 0))
	{
		TimedOut_11 = false;
		Timer_11 = millis();
		FL_Delay = true;		
	}
}