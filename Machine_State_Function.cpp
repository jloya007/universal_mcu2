/*
 * Machine_State_Function.cpp
 * Created: 29-6-2018 12:26:37
 *  Author: RK
 */ 

//Machine State Description:
//Machine_Init_State: Machine Startup.
//MAchine_Idle_State: Machine is running Idle without or without running a program program.
//Machine_Running_State: Hydraulic cylinder is moving (Down or Up) with or without running a program.

//Includes.
#include "Main_Siemens_Automation_824MSPe.h"

//Local Variables.

//Function Code.
void Machine_State_Function(void)
{	
	//Machine initialization State. Startup.
	if (Timer1_Q == true)
	{
		Machine_Init_State = true;
	}
	//Machine mode Idle.
	if ((Machine_Init_State == true) && (Robot_Mode_Active_MCU2 == false || digitalRead(FOOT_DOWN1) == LOW || digitalRead(FOOT_DOWN2) == LOW || digitalRead(MONITORING_DOWN1) == LOW || digitalRead(DOWN_SOLENOID) == LOW))
	{
		Machine_Run_State = false;
		Machine_Idle_State = true;
	}
	//Machine Running mode.
	if ((Machine_Init_State == true) && ((Robot_Mode_Active_MCU2 == true || digitalRead(FOOT_DOWN1) == HIGH && digitalRead(FOOT_DOWN2) == HIGH) && digitalRead(MONITORING_DOWN1) == HIGH && digitalRead(DOWN_SOLENOID) == HIGH) || Up_Signal_MCU2 == true)
	{
		Machine_Idle_State = false;
		Machine_Run_State = true;
	}
}