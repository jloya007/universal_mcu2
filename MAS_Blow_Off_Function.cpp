/*
 * MAS_Blow_Off_Function.cpp
 * Created: 12-7-2018 13:41:50
 *  Author: RK
 */ 

//Includes
#include "Main_Siemens_Automation_824MSPe.h"

//Local Variables.

//Timer Variables.
unsigned long Timer_4;
//Set timer to true when timer is finished.
bool TimedOut_4 = false;
//Amount of time the Timer is active.
unsigned long INTERVAL_4;

//HMI Modbus Variables.

//Setup Code.
void Setup_MAS_Blow_Off_Function(void)
{
	//Timer initialization.
	TimedOut_4 = false;
	//Start Timer.
	Timer_4 = millis();
}

//Function Code.
void MAS_Blow_Off_Function(void)
{
	if ((!TimedOut_4) && ((millis() - Timer_4) > INTERVAL_4))
	{
		//Timed out to deactivate timer.
		TimedOut_4 = true;

		//Check if MAS Blow Off is activated.
		if (MAS_Blow_Off_Active == true)
		{
			//De-active MAS Blow Off Timer.
			MAS_Blow_Off_Active = false;
			digitalWrite(MAS1_BLOWOFF, LOW);
			digitalWrite(MAS1_EJECT, LOW);
		}
	}
	//Set MAS Blow Off Time.
	INTERVAL_4 = MAS_Blow_Off_Time_HMI;
	
	//Timer Start. Activate MAS Blow Off.
	if (MAS_Feed_Fastener_Active == true && (INTERVAL_4 > 0))
	{
		TimedOut_4 = false;
		Timer_4 = millis();
		MAS_Blow_Off_Active = true;
	}
	
	//Blow Off starts after MAS Eject.
	if (!MAS_Feed_Fastener_Active == true && MAS_Blow_Off_Active == true)
	{
		digitalWrite(MAS1_BLOWOFF, HIGH);
	}
}