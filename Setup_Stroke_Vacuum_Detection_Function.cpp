/*
 * Setup_Stroke_Function.cpp
 * Created: 4-7-2018 10:50:55
 *  Author: RK
 */ 

//Includes
#include "Main_Siemens_Automation_824MSPe.h"

//Local Variables.
bool Vacuum_Measurment_Done;
bool Vacuum_Without_Fastener_Set;
bool Vacuum_With_Fastener_Set;
int VacCountWithoutFast;
int VacResultWithoutFast;
int VacCountWithFast;
int VacResultWithFast;
int Vacuum_Without_Fastener;
int Vacuum_With_Fastener;

//Vacuum Switch value.
int VacValue_Setup_Stroke_Function;

//CET set value.
int CetValue_Setup_Stroke_Function;

//Function Code.
void Setup_Stroke_Vacuum_Detection_Function(void)
{
//Read analog CET value.
CetValue_Setup_Stroke_Function = Analog_CET;

//Read analog value vacuum switch.
VacValue_Setup_Stroke_Function = Analog_VAC;
	
//Active-De-active Setup Stroke.
	//if (Setup_Stroke_HMI == 1)
	if (Fastener_Detection_HMI == true && (Setup_Stroke_HMI == true && !TPS_Set_HMI == true))
	{	
		//Activate Vacuum Measurement.
		Vacuum_Measurment_Done = false;
		//Vacuum Active to ensure no error message will displayed during setup.
		Vacuum_Detection_Measurement_Active = true;
			
		//Set Vacuum Detection Base Line.
		if (digitalRead(VACUUM_SOLENOID) == HIGH) 
		{	
			if (!Vacuum_Measurment_Done == true)
			{					
				//Measure Vacuum without Fastener.
				if ((CetValue_Setup_Stroke_Function > Fastener_Detection_Shuttle_Calibration_Pos_HMI) && !Vacuum_Without_Fastener_Set == true)
				{
					//VacCounter.
					if (VacCountWithoutFast < 1000)
					{
						VacCountWithoutFast = VacCountWithoutFast + 1;
						Vac_Without_Fastener_Down_Hold = true;
					}
					if (VacCountWithoutFast == 0)
					{
						VacResultWithoutFast = 0;
					}
					if (VacCountWithoutFast == 1000)
					{
						Vacuum_Without_Fastener = VacResultWithoutFast/VacCountWithoutFast;
						Vacuum_Without_Fastener_Set = true;
						Vac_Without_Fastener_Down_Hold = false;
					}
					//Calculate average over 1000 measurements.
					VacResultWithoutFast = VacResultWithoutFast + VacValue_Setup_Stroke_Function;
				}
					//Measure Vacuum with Fastener.
				if (Vacuum_Without_Fastener_Set == true && (CetValue_Setup_Stroke_Function < (Fastener_Detection_Shuttle_Calibration_Pos_HMI - Fastener_Detection_Shuttle_Offset_Pos_HMI)) && !Vacuum_With_Fastener_Set == true)
				{
					//VacCounter.
					if (VacCountWithFast < 1000)
					{
						VacCountWithFast = VacCountWithFast + 1;
						Vac_With_Fastener_Down_Hold = true;
					}
					if (VacCountWithFast == 0)
					{
						VacResultWithFast = 0;
					}
					if (VacCountWithFast == 1000)
					{
						Vacuum_With_Fastener = VacResultWithFast/VacCountWithFast;
						Vacuum_With_Fastener_Set = true;
						Vac_With_Fastener_Down_Hold = false;
					}
					//Calculate average over 1000 measurements.
					VacResultWithFast = VacResultWithFast + VacValue_Setup_Stroke_Function;
				}
				//Generate Error message when Fastener is available during Setup Stroke. Vacuum reading is higher below Shuttle (because of Vacuum Build up)
				if (Vacuum_With_Fastener_Set == true && ((Vacuum_With_Fastener - 100) <= Vacuum_Without_Fastener))
				{
					Fastener_Detection_Error = true;
					Vacuum_Without_Fastener_Set = false;
					Vacuum_With_Fastener_Set = false;
					VacCountWithoutFast = 0;
					VacCountWithFast = 0;
					VacResultWithFast = 0;
					VacResultWithoutFast = 0;
					
				}
				//Set Fastener Detection Base Line.
				if(Vacuum_With_Fastener_Set == true && (Vacuum_With_Fastener > Vacuum_Without_Fastener))
				{
					Fastener_Detection_Baseline_HMI = Vacuum_Without_Fastener + ((Vacuum_With_Fastener - Vacuum_Without_Fastener)/2);
					Setup_Stroke_HMI = 0;
					Vacuum_Measurment_Done = true;
					Vacuum_Without_Fastener_Set = false;
					Vacuum_With_Fastener_Set = false;
					VacCountWithoutFast = 0;
					VacCountWithFast = 0;
				}
			}
		} 
	}
	//Reset Vacuum Measurement and Vacuum Detection Measurement Active.
	else
	{
		Vacuum_Measurment_Done = false;
		Vacuum_Detection_Measurement_Active = false;
		Vacuum_With_Fastener = 0;
		Vacuum_Without_Fastener = 0;
		VacResultWithFast = 0;
		VacResultWithoutFast = 0;
	}
}



