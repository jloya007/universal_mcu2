/*
 * Press_Complete_Function.cpp
 * Created: 12-7-2018 08:00:27
 *  Author: RK
 */ 

//Includes
#include "Main_Siemens_Automation_824MSPe.h"

//Local Variables.
//Ref Force 618MSPE = 200, 824MSPE = 150.
int Force_Ref_Value = 200;
int Pressure_Switch_AI;
bool Ref_Press_Reached;

//Timer Variables.
unsigned long Timer_1;
//Set timer to true when timer is finished.
bool TimedOut_1 = false;
//Press Complete Signal Active Time 500ms.
unsigned long INTERVAL_1 = 700;

//Setup Code.
void Setup_Press_Complete_Function(void)
{
	//Timer initialization.
	TimedOut_1 = false;
	//Start Timer.
	Timer_1 = millis();
}

//Function Code.
void Press_Complete_Function (void)
{
	//Press complete ref value.
	Pressure_Switch_AI = Analog_PRESSURESWITCH;
	
	//Turn Press Complete Signal on for 200ms.
	if ((!TimedOut_1) && ((millis() - Timer_1) > INTERVAL_1))
	{
		//Timed out to deactivate timer.
		TimedOut_1 = true;

		//Check if Press Complete is activated.
		if (Press_Complete == true)
		{
			//De-active Press Complete.
			Press_Complete = false;
		}
	}
	//Timer Start. Activate Press Complete.
	//if (((Pressure_Switch_AI > Force_Ref_Value) && Safety_Sensor_Switched_State == true) && (INTERVAL_1 > 0) && !Hyd_Cyl_Up_Release_Active == true && !Ref_Press_Reached == true)
	if (((Pressure_Switch_AI > Force_Ref_Value) && Safety_Sensor_Switched_State == true) && (INTERVAL_1 > 0) && !Hyd_Cyl_Up_Release_Active == true && !Ref_Press_Reached == true && !Non_Conductive_Stop1 == true)
	{
		TimedOut_1 = false;
		Timer_1 = millis();
		Press_Complete = true;
		Ref_Press_Reached = true;
	}
	//Reset Ref Pressure.
	if (Pressure_Switch_AI < Force_Ref_Value)
	{
		Ref_Press_Reached = false;
	}
}