/*
 * Vacuum_Generator_Function.cpp
 * Created: 20-7-2018 10:51:21
 *  Author: RK
 */ 

 //Includes
 #include "Main_Siemens_Automation_824MSPe.h"

 //Local Variables.
 bool Vacuum_Active;
 
 //CET set value.
 int CetValue_Vacuum_Generator_Function;

 //Function Code.
 void Vacuum_Generator_Function(void)
 {
	 //Read analog CET value.
	 CetValue_Vacuum_Generator_Function = Analog_CET;
	 
	 //Vacuum Mode 0 = Auto.
	 //Vacuum Mode 1 = Off.
	 //Vacuum Mode 2 = On.
	if ((digitalRead(DOWN_SOLENOID) == HIGH && Vacuum_Mode_HMI == 0) || (Vacuum_Mode_HMI == 2) &&! (Error_Number_HMI > 0)) 
	{
		Vacuum_Active = true;
	}
	if ((Safety_Sensor_Switched_State == true) || (Vacuum_Mode_HMI == 1)||(Tooling_Mode_HMI == 0) || (Tooling_Mode_HMI == 1) || (!Vacuum_Mode_HMI == 2) || (Error_Number_HMI > 0))
	{
		Vacuum_Active = false;
	}
	//Activate/Deactivate Vacuum.
	if (Vacuum_Active == true)
	{
		digitalWrite(VACUUM_SOLENOID, HIGH);
	}
	else
	{
		digitalWrite(VACUUM_SOLENOID, LOW);
	}
}