/*
 * Setup_Stroke_Function.cpp
 * Created: 4-7-2018 10:50:55
 *  Author: RK
 */ 

//Includes
#include "Main_Siemens_Automation_824MSPe.h"

//Local Variables.
bool Stop_Read_TPS_U_Value;
bool Init_TPS_Values;

//Function Code.
void Setup_Stroke_TPS_Function(void)
{
	//Active-De-active Setup Stroke.
	if (Setup_Stroke_HMI == true && !TPS_Set_HMI == true)
	{		
		//Active FLD Measurement.
		FLD_Measurment_Active = true;
			
		if (Init_TPS_Values == true)
		{
			TPS_Calibrated_Upper_Value_HMI = 0;
			TPS_Calibrated_Lower_Value_HMI = 0;
		}	
		//3th Step: Detect Tooling Protection Position when Safety Sensors switch State. TPS is always active.
		if (Safety_Sensor_Switched_State == true && !Stop_Read_TPS_U_Value == true)
		{
			Init_TPS_Values = false;
			TPS_Calibrated_Upper_Value_HMI = Analog_CET;
			Stop_Read_TPS_U_Value = true;
		}
		if (Safety_Sensor_Switched_State == true && Press_Complete == true)
		{
			TPS_Calibrated_Lower_Value_HMI = Analog_CET;
			TPS_Set_HMI = true;
			FL_Setup_Stroke_Reset = false;
		}
	}
	//Reset TPS Measurement Active.
	else
	{
		Stop_Read_TPS_U_Value = false;
		Init_TPS_Values = true;
	}
	//Read Setup Stroke and TPS is set from HMI. HMI will set when Setup stroke is ready.
	if (TPS_Set_HMI == true)
	{
		Write_Coil_34 = false;
		Read_Coil_34 = true;
		Write_Coil_40 = false;
		Read_Coil_40 = true;
	}
	//Check if Setup stroke is activated.
	if (Setup_Stroke_HMI == true)
	{
		Read_Coil_40 = false;
		Write_Coil_40 = true;
		//Activate Second foot pedal force confirmation.
		Setup_Stroke_TPS = true;
	}
	else if (!Setup_Stroke_HMI == true)
	{
		Write_Coil_34 = false;
		Read_Coil_34 = true;
		//Deactivate Second foot pedal force confirmation.
		Setup_Stroke_TPS = false;
	}
	//TPS setup send to MCU2. To de-active Down pedal at MCU2.
	if (Setup_Stroke_TPS == true)
	{
		Setup_Stroke_MCU2 = true;
	}
	else
	{
		Setup_Stroke_MCU2 = false;
	}
}


