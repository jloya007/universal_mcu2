/*
 * Modbus_Communication_Function.cpp
 * Created: 11-8-2018 08:19:53
 *  Author: RK
 */ 
//Includes
#include "Main_Siemens_Automation_824MSPe.h"

//Local Variables.
int Modbus_Swap_Bit_Time = 100;
bool Swap_Bit;

//Timer Variables.
unsigned long Timer_8;
//Set timer to true when timer is finished.
bool TimedOut_8 = false;
//Amount of time the Timer is active.
unsigned long INTERVAL_8;

//Setup Code.
void Setup_Modbus_Communication_Function(void)
{
	//Timer initialization.
	TimedOut_8 = false;
	//Start Timer.
	Timer_8 = millis();
}

//Function Code.
void Modbus_Communication_Function(void)
{
	//Bits which needs to be swapped.
	if ((TimedOut_8 == true) && ((millis() - Timer_8) > INTERVAL_8))
	{
		//Timed out to deactivate timer.
		TimedOut_8 = true;

		//Check if Swap Bit is activated.
		if (Swap_Bit == true)
		{
			//De-active Timer.
			Swap_Bit = false;
			TimedOut_8 = false;
		}
	}
	//Set Swap Bit Time.
	INTERVAL_8 = Modbus_Swap_Bit_Time;
			
	//Timer Start.
	if (MAS_Feed_HMI == 1 && TimedOut_8 == false)
	{
		TimedOut_8 = true;
		Timer_8 = millis();
		Swap_Bit = true;
	}
	
	//Swap Mas Feed_HMI bit.
	if (Swap_Bit == true && MAS_Feed_HMI == true)
	{
		MAS_Feed_HMI = 0;
		Read_Coil_38 = false;
		Write_Coil_38 = true;
	}
	else
	{
		Write_Coil_38 = false;
		Read_Coil_38 = true;
	}
}