/*
 * Fastener_Detection_Function.cpp
 * Created: 11-7-2018 11:11:51
 *  Author: RK
 */ 

//Includes
#include "Main_Siemens_Automation_824MSPe.h"

//Local Variables.
int FDCount = 0;
bool FDCount_Disable;

//Timer Variables.
unsigned long Timer_10;
//Set timer to true when timer is finished.
bool TimedOut_10 = false;
//Amount of time the Timer is active.
unsigned long INTERVAL_10;
//Fastener Detection Delay Var.
bool FD_Delay;
//Fastener Detection Delay Time.
int FD_Delay_Time = 800;

//Vacuum Switch value.
int VacValue_Fastener_Detection_Function;

//CET set value.
int CetValue_Fastener_Detection_Function;

//Setup Code.
void Setup_Fastener_Detection_Function(void)
{
	//Timer initialization.
	TimedOut_10 = false;
	//Start Timer.
	Timer_10 = millis();
}

//Function Code.
void Fastener_Detection_Function(void)
{
	//Read analog CET value.	
	CetValue_Fastener_Detection_Function = Analog_CET;

	//Read analog value vacuum switch.
	VacValue_Fastener_Detection_Function = Analog_VAC;
	
	//Fastener Detection Shuttle Position.
	Fastener_Detection_Zone = (Fastener_Detection_Shuttle_Calibration_Pos_HMI - Fastener_Detection_Shuttle_Offset_Pos_HMI);
	
	if (Fastener_Detection_HMI == true && !Vacuum_Detection_Measurement_Active == true && Tooling_Mode_HMI == 2)
	{
		//Fastener detection value from Vacuum Sensor needs to be higher then Fastener Detection calibrated value from HMI + Fastener detection sensitivity at Shuttle position. Shuttle reference 1mm above shuttle.
		if ((!Fastener_Detection_Error == true) && (CetValue_Fastener_Detection_Function > Fastener_Detection_Zone) && ((Fastener_Detection_Shuttle_Calibration_Pos_HMI-100) > CetValue_Fastener_Detection_Function) && (VacValue_Fastener_Detection_Function < (Fastener_Detection_Baseline_HMI - Fastener_Detection_Sensitivity_HMI)) && (digitalRead(DOWN_SOLENOID) == HIGH) || FD_Delay == true)
		{
			Fastener_Detection_Error = true;
		}
	}
	//Read coils. Reset FD error. MCU is master at reset. HMI will get only ACK.
	if (!Safety_Sensor_Error == true && !SSSF_Safety_Sensor_Error == true && !TPS_Error == true && !Fastener_Detection_Error == true && !Fastener_Length_Error == true && !FD_Retry_Set == true && !Shuttle_Extend_Error == true && !Shuttle_Retract_Error == true)
	{
		Error_Set_HMI = 0;
		Error_Number_HMI = 0;
	}
	//Write coils. Set HMI into error mode. Display TPS error by error number 16.
	if (FD_Retry_Set == true)
	{
		Read_Coil_48 = false;
		Write_Coil_48 = true;
		Read_Coil_73 = false;
		Write_Coil_73 = true;
		Error_Set_HMI = 1;
		Error_Number_HMI = 16;
	}
	//Fastener Detection Retry.
	if (Fastener_Detection_Error == true && !FDCount_Disable == true)
	{
		FDCount_Disable = true;
		FDCount = FDCount + 1;
	}
	//Active Fastener Detection Error.
	if ((FDCount == Fastener_Detection_Retry_HMI) && !Fastener_Detection_Error == false)
	{
		FD_Retry_Set = true;
		FDCount = 0;
	}
	//Reset Fastener Detection Counter.
	else if (Fastener_Detection_Error == false)
	{
		FDCount_Disable = false;
		FD_Retry_Set = false;
	}	
	if (Press_Complete == true)
	{
		FDCount = 0;
	}
	//Fastener Detection Timer. Delay Error Reset Signal (500ms).
	if ((!TimedOut_10) && ((millis() - Timer_10) > INTERVAL_10))
	{
		//Timed out to deactivate timer.
		TimedOut_10 = true;

		//Check if FD Delay is activated.
		if (FD_Delay == true)
		{
			//De-active Timer.
			FD_Delay = false;
		}
	}
	//Set FD Delay Time.
	{
		INTERVAL_10 = FD_Delay_Time;
	}
	//Timer Start. Activate FD Delay Timer.
	if (Fastener_Detection_Error == true && (CetValue_Fastener_Detection_Function < TOS) && (INTERVAL_10 > 0))
	{
		TimedOut_10 = false;
		Timer_10 = millis();
		FD_Delay = true;
	}	
}