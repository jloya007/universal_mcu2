/*
 * All_At_Position_Function.cpp
 * Created: 10-9-2018 19:05:28
 *  Author: RK
 */ 

//Includes
#include "Main_Siemens_Automation_824MSPe.h"

//Local Variables.
bool Deactivate_All_At_Position;
bool Fastener_Detection_Error_Lock1;
bool Fastener_Detection_Error_Lock2;
bool Shuttle_Extend_Check;

//CET set value.
int CetValue_All_At_Position_Function;

//Function Code.
void All_At_Position_Function(void)
{
	//Read analog CET value.
	CetValue_All_At_Position_Function = Analog_CET;
	
	if(Machine_State_Robot_Mode_Active == true)
	{
		//Activate/deactivate All At Position.
		if (EMG_System_Active_HMI == true && !Error_Set_HMI == true && !Up_Signal_MCU2 == true && !Deactivate_All_At_Position == true && !digitalRead(MAS1_BLOWOFF) == HIGH && (!Fastener_Detection_Error_Lock1 == true && !Fastener_Detection_Error_Lock2 == true) && Shuttle_Extend_Check == true || Dwell_Time_Active == true)
		{
			All_At_Position = true;
		}
		else
		{
			Deactivate_All_At_Position = true;
		}	
		//Deactivate All at Position at TOS.
		if (Deactivate_All_At_Position == true)
		{
			Deactivate_All_At_Position = true;
			All_At_Position = false;
		}
		//Activate All at Position at TOS if all other conditions are false.
		if (CetValue_All_At_Position_Function >= TOS)
		{
			Deactivate_All_At_Position = false;
		} 
	}
	else
	{
		All_At_Position = false;
	}
	//Disable All At Position during Fastener Detection Error.
	if (Fastener_Detection_Error == true)
	{
		Fastener_Detection_Error_Lock1 = true;
	}
	if (digitalRead(MAS1_BLOWOFF) == HIGH)
	{
		Fastener_Detection_Error_Lock2 = true;
		Fastener_Detection_Error_Lock1 = false;
	}
	else
	{
		Fastener_Detection_Error_Lock2 = false;
	}
	//Ensure that Shuttle is Extended after Fastener Detection Error before All At Position is Set.
	//if (digitalRead(SHUTTLE_EXTENDED) == HIGH && !Shuttle_Extend_Check == true)
	if (digitalRead(SHUTTLE_EXTENDED) == HIGH)
	{
		Shuttle_Extend_Check = true;
	}
	//Shuttle Extend is false.
	if ((Tooling_Mode_HMI == 2) && digitalRead(SHUTTLE_EXTENDED) == LOW && (CetValue_All_At_Position_Function >= TOS))
	{
		Shuttle_Extend_Check = false;
	}
}